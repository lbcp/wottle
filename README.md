# Speedle

### Speedle is a wordle clone that you can play against your friends.
Simply connect to the same WiFi network, share your local IP and figure out who can find the hidden word first.

### Being in the park without WiFi?
No problem, simply use your phones WiFi hotspot capabilities and enjoy playing against your friends everywhere. You don't need to be connected to the internet. You just need to be in the same network.

### No friends in sight?
You can play the Daily Speedle and share your results via Signal, Whatsapp or whatever messenger you like.

### Still craving for yet another game?
With infinity speedle, you have 5 minutes to find the word. For every revealed word, you get 60 seconds. How many words can you find before the time runs out?

### That's all too stressful?
Play as many solo games as you wish. During solo games you can disable the timer and take your time.

## How to download the App?
You can find the most recent builds as Job artifacts CI/CD section.
Alternatively, check out the Web App at https://lbcp.gitlab.io/wottle (Mutliplayer does not work in Browser)

The App is now available on Google Play.

<a href='https://play.google.com/store/apps/details?id=org.lbcp.Speedle&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' height="130"/></a>
