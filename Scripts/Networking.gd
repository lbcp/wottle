extends Node

signal reconnected

func _ready():
	hide_panels()
# warning-ignore:return_value_discarded
	gamestate.connect("connection_succeded", self, "_pre_start_game")
# warning-ignore:return_value_discarded
	gamestate.connect("connection_failed", self, "_couldnt_connect")
	$Panel/VBoxContainer/HBoxContainer/IP_Address.text = "192.168."


func _process(delta):
	if self.visible:
		var my_ip
		for i in IP.get_local_addresses():
			if "192.168." in i:
				my_ip = i
		if my_ip:
			$Panel/HBoxContainer/OwnIP.text = my_ip
			$WaitingPanel/HBoxContainer/OwnIP.text = my_ip
		else:
			$Panel/HBoxContainer/OwnIP.text = "Could not determine IP"
			$Panel/VBoxContainer/HBoxContainer/IP_Address.text = ""
		

func _on_Host_Button_pressed():
	set_own_name()
	hide_panels()
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(10567, 2)
	get_tree().network_peer = peer
	$WaitingPanel.show()


func _on_Join_Button_pressed():
	set_own_name()
	hide_panels()
	var peer = NetworkedMultiplayerENet.new()
	var server_address = $Panel/VBoxContainer/HBoxContainer/IP_Address.text
	if server_address.is_valid_ip_address():
		peer.create_client(server_address, 10567)
		get_tree().network_peer = peer
		$Panel/VBoxContainer/JoinWaitControl.show()
	else:
		$Panel/VBoxContainer/HBoxContainer/IP_Address.text = ""
		$Panel/VBoxContainer/HBoxContainer/IP_Address.placeholder_text = "Invalid IP address"


func try_reconnect():
	var peer = NetworkedMultiplayerENet.new()
	var server_address = $Panel/VBoxContainer/HBoxContainer/IP_Address.text
	if server_address.is_valid_ip_address():
		peer.create_client(server_address, 10567)
		get_tree().network_peer = peer


func set_own_name():
	if $Panel/VBoxContainer/Name_Container/Player_Name.text != "":
		gamestate.my_name = $Panel/VBoxContainer/Name_Container/Player_Name.text
	else:
		gamestate.my_name = "Randy Random"

func _pre_start_game():
	hide_panels()
	if get_parent().has_node("WordsFields"):
		emit_signal("reconnected")
	else:
		get_node("..").start_multiplayer_game()

func _on_CancelServer_pressed():
	hide_panels()
	get_tree().network_peer = null
	$"../MainMenu".show()
	$".".hide()

func _couldnt_connect():
	$ConnectionFailed.show()

func hide_panels():
	$Panel/VBoxContainer/JoinWaitControl.hide()
	$ConnectionFailed.hide()
	$WaitingPanel.hide()

func _on_BackButton_pressed():
	_on_CancelServer_pressed()
