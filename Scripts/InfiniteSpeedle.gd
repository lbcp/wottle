extends "res://Scripts/WordsField.gd"


var previous_words = []
var streak : int = 0
var max_streak : int = 0 setget max_streak_set
var words : Array
var saved = false


func _ready():
	$GameEndWindow.hide()
	$OpponentProgress.hide()
	$TimerLabel/SoloGameTimer.start()
	$TimerLabel.show()
	update_stats()


func select_word(_letters):
	randomize()
	var file = File.new()
	file.open("res://Assets/Words/fiveLetters_word_list_english.csv", File.READ)
	while !file.eof_reached():
		var content = file.get_csv_line()
		words.append(content[0].to_lower())
		if not content[0].to_lower() in wordlist:
			wordlist.append(content[0].to_lower())
	file.close()
	words.shuffle()
	return words[0]


func end_game(result):
	if gamestate.sound_on:
		if result == "win":
			$WinAudio.play()
		else:
			$LooseAudio.play()
	var game_type = "Infinite"
	if result == "win" and streak != len(words):
		time_left += 60
		streak += 1
		$Hibernate.start()
		$PlusTimeLabel/AnimationPlayer.play("AddTime")
		update_stats()
		#clear_board()
		return
		
	$TimerLabel/SoloGameTimer.stop()
	keyboard.disable_keys()
	$VBoxContainer/MarginContainer/Error_Label.text = required_word
	$VBoxContainer/MarginContainer/Error_Label/AnimationPlayer.play("Show")
	yield($VBoxContainer/MarginContainer/Error_Label/AnimationPlayer, "animation_finished")
	$GameEndWindow.show()
	if streak == len(words):
		$GameEndWindow/Panel/VBoxContainer/Label.text = "You solved all words! Amazing."
	elif result == "timeout":
		$GameEndWindow/Panel/VBoxContainer/Label.text = "Time has run out."
	elif result == "defeat":
		$GameEndWindow/Panel/VBoxContainer/Label.text = "No more guesses left."
	
	if streak > max_streak:
		$GameEndWindow/Panel/VBoxContainer/Label.text += "\nNew High-Score!"
		max_streak = streak
	get_node("..").store_in_dict(result, words.slice(0, streak), input_words_list, game_type, today, time_left, streak, max_streak)
	saved = true
	update_stats()


func clear_board():
	if streak > max_streak:
		$StreakContainer/VBoxContainer/StreakLabel.modulate = Color(0.2, 0.7, 0.2, 0.8)
	for row in $VBoxContainer.get_children():
		if row.get_class() != "HBoxContainer":
			continue
		for field in row.get_children():
			field.get_node("OwnLabel").text = ""
			field.get_node("OwnLabel/LabelBackground").color = Color(0, 0, 0, 1)
	$VKeyboard.reset_colours()
	game_round = 0
	required_word = words[streak]


func update_stats():
	var minutes = time_left / 60
	var seconds = time_left % 60
	$TimerLabel.text = "Time left: " + "%01d:%02d" % [minutes, seconds]
	$StreakContainer/VBoxContainer/StreakLabel.text = str(streak)
	$StreakContainer/VBoxContainer2/MaxStreakLabel.text = str(max_streak)


func max_streak_set(value):
	max_streak = value
	update_stats()


func _on_playagainButton_pressed():
	if not saved:
		if streak > max_streak:
			max_streak = streak
		get_node("..").store_in_dict("aborted", words.slice(0, streak), input_words_list, "Infinite", today, time_left, streak, max_streak)
	get_parent().start_infinite_game()
	self.queue_free()


func back_to_menu():
	if not saved:
		if streak > max_streak:
			max_streak = streak
		get_node("..").store_in_dict("aborted", words.slice(0, streak), input_words_list, "Infinite", today, time_left, streak, max_streak)
	get_node("..").get_node("MainMenu").show()
	self.queue_free()


func _on_Hibernate_timeout():
	hard_mode_dict = {"yellows": [], "greens": []}
	clear_board()
