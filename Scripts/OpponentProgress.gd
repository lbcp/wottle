extends Control

const HBox_container = preload("res://Scenes/Opponent_Panel_Hbox.tscn")

func _ready():
	$ConnectionErrorPanel.hide()
	gamestate.connect("opponent_disconnected", self, "_op_disconnect")
	get_node("../../Networking").connect("reconnected", self, "_reconnected")
	for i in range(6):
		var hbox = HBox_container.instance()
		$VBoxContainer.add_child(hbox)


func _on_WordsFields_guess_made(op_guess):
	Input.vibrate_handheld(500)
	if gamestate.sound_on:
		$AudioStreamPlayer.play()
	for i in range(len(op_guess)):
		for j in range(len(op_guess[i])):
			$VBoxContainer.get_child(i).get_child(j).modulate = op_guess[i][j]


func _op_disconnect():
	$ConnectionTimeoutTimer.start()


remote func reveal_op_guesses(op_guesses):
	for i in range(len(op_guesses)):
		for j in range(len(op_guesses[i])):
			$VBoxContainer.get_child(i).get_child(j).get_child(0).text = str(op_guesses[i][j])


func _on_revealButton_pressed():
	rpc("reveal_op_guesses", get_parent().input_words_list)


func _reconnected():
	$ConnectionTimeoutTimer.stop()
	$ConnectionTimeoutTimer.wait_time = 7
	$ConnectionErrorPanel.hide()
	get_node("..").get_node("GameEndWindow/Panel/VBoxContainer/HBoxContainer/playagainButton").disabled = false
	get_parent().rpc("send_guess", get_parent().guess_result_history)
	get_parent().rpc("ask_guesses")


func _on_ConnectionTimeoutTimer_timeout():
	$ConnectionErrorPanel.show()
	get_node("..").network_game = false
	get_node("..").get_node("GameEndWindow/Panel/VBoxContainer/HBoxContainer/playagainButton").disabled = true
