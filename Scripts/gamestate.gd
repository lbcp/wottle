extends Node


# From Bomberman tutorial
# Default game server port. Can be any number between 1024 and 49151.
# Not on the list of registered or common ports as of November 2020:
# https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
const DEFAULT_PORT = 10567

# Max number of players.
const MAX_PEERS = 2

var peer = null
var my_name
# Names for remote players in id:name format.
var players = {}
var players_ready = []

var sound_on = false
var hard_mode = false

# Signals to let lobby GUI know what's going on.
signal connection_succeded
signal connection_failed
signal opponent_disconnected


func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")


# Callback from SceneTree.
func _player_connected(id):
	# Registration of a client beings here, tell the connected player that we are here.
	rpc_id(id, "register_player", my_name)
	

remote func register_player(new_player_name):
	var id = get_tree().get_rpc_sender_id()
	players[id] = new_player_name
	emit_signal("connection_succeded")
	
func _connected_ok():
	# Not necessary
	pass

func _player_disconnected(_who):
	emit_signal("opponent_disconnected")
	pass

func _server_disconnected():
	emit_signal("opponent_disconnected")
	pass

func _connected_fail():
	emit_signal("connection_failed")

func join_game(ip, new_player_name):
	pass


func get_player_list():
	return players.values()


func get_player_name():
	return my_name
