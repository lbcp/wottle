extends Control
class_name GameBoard

const Label_rect = preload("res://Scenes/Label_rect2.tscn")

signal guess_made

remote var required_word: String = ""

var game_round:int = 0
var letter:int = 0
var max_letters:int = 5
var wordlist = []
var green = Color(0.2, 0.7, 0.2, 0.9)
var yellow = Color(0.9, 0.9, 0.1, 0.7)
var grey = Color(0.3, 0.3, 0.3, 0.8)
var input_words_list = []
var guess_result_history = {}
var network_game = true
var result_sent = false
var opponent_result
var hard_mode = false
var hard_mode_dict = {"yellows": [], "greens": []}
remote var opponent_name : String
var ilost = false
var today
export var time_left = 300
remote var new_game = false

onready var keyboard = get_node("VKeyboard")

func _ready():
	today = OS.get_date(true)
	today = "%04d-%02d-%02d" % [today.year, today.month, today.day]
	$OpponentProgress.show()
	$GameEndWindow.hide()
	$GameEndWindow/Panel/Opponent_reaction.hide()
	retrieve_wordlist()
	draw_board()
	set_timer()
	if get_tree().network_peer == null:
		network_game = false
		hard_mode = gamestate.hard_mode
		$OpponentProgress.hide()
		required_word = select_word(max_letters)
		if get_node("../MainMenu/TimerDisabled").pressed: # TODO: Rename the node
			$TimerLabel/SoloGameTimer.start()
			$TimerLabel.show()
		else:
			$TimerLabel.hide()
	elif get_tree().is_network_server():
		required_word = select_word(max_letters)
		get_tree().set_pause(true)
	else:
		rpc_id(1, "confirm_ready", gamestate.my_name)
		get_tree().set_pause(true)


func draw_board():
	for row in $VBoxContainer.get_children():
		if row.get_class() != "HBoxContainer":
			continue
		for _j in range(max_letters):
			var label = Label_rect.instance()
			label.get_child(0).text = ""
			row.add_child(label)


func retrieve_wordlist():
	wordlist = get_parent().all_words


func select_word(letters):
	randomize()
	var file = File.new()
	var words = []
	if letters == 5:
		file.open("res://Assets/Words/fiveLetters_word_list_english.csv", File.READ)
	elif letters == 6:
		file.open("res://Assets/Words/sixLetters_word_list_english.csv", File.READ)
	while !file.eof_reached():
		var content = file.get_csv_line()
		words.append(content)
		if not content[0].to_lower() in wordlist:
			wordlist.append(content[0].to_lower())
	file.close()
	var random_word = words[randi() % words.size()]
	return random_word[0]

func in_wordlist(word):
	if not word in wordlist:
			return false
	else:
		return true


func check_hard_mode(word) ->  bool:
	for i in hard_mode_dict["yellows"]:
		if not i in word:
			return false
	for gr in hard_mode_dict["greens"]:
		for letter in gr:
			if word[gr[letter]] != letter:
				return false
	hard_mode_dict = {"yellows": [], "greens": []}
	return true


func check_word(word: String):
	word = word.to_lower()
	required_word = required_word.to_lower()
	input_words_list.append(word)
	var word_dict = {}
	var letter_pos_col = {}

	for l in required_word:
		if l in word_dict:
			word_dict[l] += 1
		else:
			word_dict[l] = 1
	for i in range(max_letters):
		if word[i] == required_word[i]:
			letter_pos_col[i] = green
			word_dict[word[i]] -= 1
			hard_mode_dict["greens"].append({word[i]: i}) 
		elif not word[i] in required_word:
			letter_pos_col[i] = grey
	# check for remaining yellow positions
	for i in range(max_letters):
		if word[i] in required_word and not i in letter_pos_col:
			if word_dict[word[i]] > 0:
				letter_pos_col[i] = yellow
				word_dict[word[i]] -= 1
				hard_mode_dict["yellows"].append(word[i])
			else:
				letter_pos_col[i] = grey
	keyboard.disable_keys()
	animate(word, letter_pos_col)
	keyboard.enable_keys()
	
	game_round += 1
	letter = 0
	if word == required_word:
		end_game("win")
	elif game_round > 5:
		ilost = true
		end_game("defeat")
	return letter_pos_col


func animate(word, letter_pos_col):
	var tween
	for i in range(max_letters):
		modify_letter(i, word[i], letter_pos_col[i])
		tween = $VBoxContainer.get_child(game_round).get_child(i).get_node("Tween")
	yield(tween, "tween_completed")


func modify_letter(letter_position:int, guessed_letter:String, color:Color):
	var tween = $VBoxContainer.get_child(game_round).get_child(letter_position).get_node("Tween")
	var labelBackground = $VBoxContainer.get_child(game_round).get_child(letter_position).get_node("OwnLabel/LabelBackground")
	tween.interpolate_property(labelBackground, "color", labelBackground.color, color, 1.0, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	
	# Modifying the keyboard
	for i in range(len(keyboard.letters_list)):
		if guessed_letter in keyboard.letters_list[i]:
			for l in keyboard.get_node("Vbox").get_child(i).get_children():
				if l.text == guessed_letter:
					if l.target_colour == green:
						pass
					elif (l.target_colour == yellow and not color == green):
						pass
					else:
						l.target_colour = color
						l.change_bg_colour(color)


func end_game(result):
	if gamestate.sound_on:
		if result == "win":
			$WinAudio.play()
		else:
			$LooseAudio.play()
	$TimerLabel/SoloGameTimer.stop()
	keyboard.disable_keys()
	$GameEndWindow/Panel/VBoxContainer/Button.disabled = false
	if network_game:
		$GameEndWindow/Panel/VBoxContainer/HBoxContainer/revealButton.show()
		var game_type = "Multiplayer"
		if not result_sent:
			rpc("opponent_finished", result)
			result_sent = true
		if result == "win":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "Congratulations. You won!"
		elif result == "defeat" and not opponent_result:
			$GameEndWindow/Panel/VBoxContainer/Label.text = "You failed. Waiting for " + opponent_name + "."
			$GameEndWindow/Panel/VBoxContainer/Button.disabled = true
		elif result == "defeat" and opponent_result == "win":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "You lost. " + opponent_name + " solved it."
		elif result == "defeat" and opponent_result == "defeat":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "Tie game. You both lost."
		elif result == "opponent_was_faster":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "You lost. " + opponent_name + " was faster."
	# More things need to be changed depending on win or loose
	# Like changing the colour or playing a sound or so.
	else:
		var game_type = "Random"
		$GameEndWindow/Panel/VBoxContainer/HBoxContainer/revealButton.hide()
		if result == "win":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "You won!"
		elif result == "defeat":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "You lost!"
		elif result == "timeout":
			$GameEndWindow/Panel/VBoxContainer/Label.text = "You lost! Time has run out"
		get_node("..").store_in_dict(result, required_word, input_words_list, game_type, today, time_left)
	$VBoxContainer/MarginContainer/Error_Label.text = required_word
	$VBoxContainer/MarginContainer/Error_Label/AnimationPlayer.play("Show")
	yield($VBoxContainer/MarginContainer/Error_Label/AnimationPlayer, "animation_finished")
	$GameEndWindow.show()


master func confirm_ready(name):
	var who = get_tree().get_rpc_sender_id()
	opponent_name = name
	rset("required_word", required_word)
	rset("opponent_name", gamestate.my_name)
	rpc("unpause")


remotesync func unpause():
	get_tree().set_pause(false)


remote func send_guess(my_guesses):
	emit_signal("guess_made", my_guesses)


remote func ask_guesses():
	get_node("GameEndWindow/Panel/VBoxContainer/HBoxContainer/playagainButton").disabled = false
	get_node("OpponentProgress/ConnectionErrorPanel").hide()
	rpc("send_guess", guess_result_history)


remote func opponent_finished(op_result):
	opponent_result = op_result
	$GameEndWindow/Panel/VBoxContainer/Button.disabled = false
	if op_result == "win" and not ilost:
		end_game("opponent_was_faster")
	else:
		end_game("defeat")


func _button_pressed(text):
	if text == "Enter":
		var guessed_word = ''
		for i in $VBoxContainer.get_children()[game_round].get_children():
			guessed_word += i.get_child(0).text
		if len(guessed_word) == max_letters and in_wordlist(guessed_word):
			if hard_mode:
				if not check_hard_mode(guessed_word):
					$VBoxContainer/MarginContainer/Error_Label.text = "Known letters missing"
					$VBoxContainer/MarginContainer/Error_Label/AnimationPlayer.play("FadeInOut")
					return
			var guess_outcome = check_word(guessed_word)
			guess_result_history[game_round-1] = guess_outcome
			if not get_tree().network_peer == null:
				rpc("send_guess", guess_result_history)
		elif len(guessed_word) == max_letters and not in_wordlist(guessed_word):
			$VBoxContainer/MarginContainer/Error_Label.text = "Word not in word list"
			$VBoxContainer/MarginContainer/Error_Label/AnimationPlayer.play("FadeInOut")
		else:
			$VBoxContainer/MarginContainer/Error_Label.text = "Word too short"
			$VBoxContainer/MarginContainer/Error_Label/AnimationPlayer.play("FadeInOut")
	elif text == "Del":
		if letter == max_letters-1:
			if $VBoxContainer.get_children()[game_round].get_children()[letter].get_child(0).text == "":
				letter -= 1
			$VBoxContainer.get_children()[game_round].get_children()[letter].get_child(0).text = ""
		else:
			letter -= 1
			if letter < 0:
				letter = 0
			$VBoxContainer.get_children()[game_round].get_children()[letter].get_child(0).text = ""
	elif letter < max_letters:
		$VBoxContainer.get_children()[game_round].get_children()[letter].get_child(0).text = text
		letter += 1


func back_to_menu():
	get_node("..").get_node("MainMenu").show()
	get_tree().network_peer = null
	self.queue_free()


func _on_Button_button_up():
	back_to_menu()


func _on_BackButton_pressed():
	get_node("GameEndWindow").show()


func _on_playagainButton_pressed():
	if not network_game:
		get_parent().start_game()
		self.queue_free()
	else:
		if new_game == false:
			new_game = true
			rset("new_game", true)
			rpc("ask_restart")
			$GameEndWindow/Panel/VBoxContainer/HBoxContainer/playagainButton.disabled = true
		elif new_game:
			rpc("restart_multiplayer_game")

func set_timer():
	if gamestate.hard_mode:
		time_left += 60
	var minutes = time_left / 60
	var seconds = time_left % 60
	$TimerLabel.text = "Time left: " + "%01d:%02d" % [minutes, seconds]


func count_down():
	var minutes = time_left / 60
	var seconds = time_left % 60
	$TimerLabel.text = "Time left: " + "%01d:%02d" % [minutes, seconds]
	if time_left <= 0:
		end_game("timeout")
		$TimerLabel/SoloGameTimer.stop()
	else:
		$TimerLabel/SoloGameTimer.start()
		time_left -= 1
		$TimerLabel.text = "Time left: " + "%01d:%02d" % [minutes, seconds]


remote func ask_restart():
	$GameEndWindow/Panel/Opponent_reaction.show()
	$GameEndWindow/Panel/Opponent_reaction.text = opponent_name + " wants to play again."


remotesync func restart_multiplayer_game():
	get_parent().start_game()
	self.queue_free()


remotesync func no_more_games():
	back_to_menu()


func _on_SoloGameTimer_timeout():
	count_down()


func _on_DismissEGWButton_pressed():
	get_node("GameEndWindow").hide()
