extends "res://Scripts/WordsField.gd"

var daily_word_no : int = 0
var daily_result : String
var done_already : bool = true
var todays_game = false


func _ready():
	$ShareButton.disabled = true
	$GameEndWindow.hide()
	$OpponentProgress.hide()
	$TimerLabel/SoloGameTimer.stop()
	$VKeyboard.disable_keys()
	fill_old_data()
	update_stats()
	$TimerLabel.show()
	

func _process(delta):
	var now = OS.get_datetime(true)
	var hours_left = 23 - now["hour"]
	var minutes_left = 59 - now["minute"]
	var seconds_left = 59 - now["second"]
	$GameEndWindow/Panel/VBoxContainer/HBoxContainer2/VBoxContainer/NextSpeedleTimeLabel.text = "%02d:%02d:%02d" % [hours_left, minutes_left, seconds_left]


func fill_old_data():
	var guesses
	var result
	var previous_game = get_node("..").historic_data
	time_left = previous_game["time_left"]
	var minutes = time_left / 60
	var seconds = time_left % 60
	$TimerLabel.text = "Time left: " + "%01d:%02d" % [minutes, seconds]
	guess_result_history = previous_game["guesses"]
	result = previous_game["result"]
	if "hard" in previous_game:
		hard_mode = previous_game["hard"]
	else:
		hard_mode = false
	var tween
	var temp_guess_history = []
	for i in range(len(guess_result_history)):
		for j in range(len(guess_result_history[i])):
			$VBoxContainer.get_child(i).get_child(j).get_child(0).text = guess_result_history[i][j]
			tween = $VBoxContainer.get_child(i).get_child(j).get_node("Tween")
		temp_guess_history.append(check_word(guess_result_history[i]))
		yield(tween, "tween_completed")
	guess_result_history = temp_guess_history
	end_game(result)


func select_word(_letters):
	var previous_game = get_node("..").historic_data
	var file = File.new()
	file.open("res://Assets/Words/daily_speedle.csv", File.READ)
	for key in previous_game.keys():
		if key != "result" and key != "word" and key != "guesses" and key != "time_left"\
											 					  and key != "hard":
			today = key
			var this_day = OS.get_date(true)
			this_day = "%04d-%02d-%02d" % [this_day.year, this_day.month, this_day.day]
			if key == this_day:
				todays_game = true
			
	while !file.eof_reached():
		daily_word_no += 1
		var content = file.get_csv_line()
		if not content[0].to_lower() in wordlist:
			wordlist.append(content[0].to_lower())
		if content[1] == today:
			file.close()
			return content[0]
	file.close()
	$".".queue_free()
	return false


func end_game(result):
	if gamestate.sound_on:
		if result == "win":
			$WinAudio.play()
		else:
			$LooseAudio.play()
	$TimerLabel/SoloGameTimer.stop()
	keyboard.disable_keys()
	var game_type = "Daily"
	daily_result = result
	
	$VBoxContainer/MarginContainer/Error_Label.text = required_word
	$VBoxContainer/MarginContainer/Error_Label/AnimationPlayer.play("Show")
	$ShareButton.disabled = false


func update_stats():
	var wins = 0
	var streak = 0
	if "Daily" in get_parent().guessed_words:
		var data = get_parent().guessed_words["Daily"]
		$GameEndWindow/Panel/HBoxContainer/VBoxContainer2/GamesPlayedLabel.text = str(len(data))
		for i in range(len(data)):
			if data[i+1]["result"] == "win":
				wins += 1
				streak += 1
			else:
				streak = 0
		wins = float(wins) / len(data) * 100
	else:
		$GameEndWindow/Panel/HBoxContainer/VBoxContainer2/GamesPlayedLabel.text = "None"
	$GameEndWindow/Panel/HBoxContainer/VBoxContainer/WinsLabel.text = str(int(wins)) + "%"
	$GameEndWindow/Panel/HBoxContainer/VBoxContainer3/StreakLabel.text = str(streak)


func _on_ShareButton_pressed():
	# There are emojis below but one cant see them in the editor
	var green_rect = "🟩"
	var grey_rect = "⬛"
	var yellow_rect = "🟨"
	var sandclock = "⏳"
	var red_x = "❌"
	var muscle = "💪"
	var cliptext : String
	var minutes = time_left / 60
	var seconds = time_left % 60
	var hard_modifier
	var today_modifier : String = "Historic "
	
	if todays_game:
		today_modifier = ""
	
	if hard_mode:
		hard_modifier = " " + muscle
	else:
		hard_modifier = ""
	
	match daily_result:
		"win":
			cliptext = today_modifier + "Daily Speedle " + str(daily_word_no) + " "  + str(len(guess_result_history)) + "/6"
			cliptext += "\n" + sandclock + ": " + "%01d:%02d" % [minutes, seconds] + hard_modifier
		"defeat":
			cliptext = today_modifier + "Daily Speedle " + str(daily_word_no) + " "  + "X/6"
			cliptext += "\n" + sandclock + ": " + "%01d:%02d" % [minutes, seconds] + hard_modifier
		"timeout":
			cliptext = today_modifier + "Daily Speedle " + str(daily_word_no) + " "  + str(len(guess_result_history)) + "/6"
			cliptext += "\n" + sandclock + " " + red_x + hard_modifier
	cliptext += "\n\n"
	
	for guess in range(len(guess_result_history)):
		for colour in range(len(guess_result_history[guess])):
			match guess_result_history[guess][colour]:
				green:
					cliptext += green_rect
				yellow:
					cliptext += yellow_rect
				grey:
					cliptext += grey_rect
		cliptext += "\n"
	OS.clipboard = cliptext
	$ShareButton/ClipboardLabel/ClipboardPlayer.play("FadeOutFadeIn")


func back_to_menu():
	get_node("..").get_node("MainMenu").show()
	get_tree().network_peer = null
	self.queue_free()
