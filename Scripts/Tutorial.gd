extends Control


var green = Color(0.2, 0.7, 0.2, 0.9)
var yellow = Color(0.9, 0.9, 0.1, 0.7)
var grey = Color(0.3, 0.3, 0.3, 0.8)
var green_word = "grand"
var yellow_word = "dairy"
var final_word = "peace"


# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(len(green_word)):
		var w = $TabContainer/General/GreenField.get_child(0).get_child(i)
		var x = $TabContainer/General/YellowField.get_child(0).get_child(i)
		var y = $TabContainer/General/FinalField.get_child(0).get_child(i)
		w.get_node("OwnLabel").text = green_word[i]
		x.get_node("OwnLabel").text = yellow_word[i]
		y.get_node("OwnLabel").text = final_word[i]
		w.get_node("OwnLabel/LabelBackground").color = grey
		x.get_node("OwnLabel/LabelBackground").color = grey
		y.get_node("OwnLabel/LabelBackground").color = green
	
	$TabContainer/General/GreenField.get_child(0).get_child(2).get_node("OwnLabel/LabelBackground").color = green
	$TabContainer/General/YellowField.get_child(0).get_child(1).get_node("OwnLabel/LabelBackground").color = yellow


func remove():
	self.queue_free()
