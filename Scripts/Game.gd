extends Control

const WordsField = preload("res://Scenes/WordsField.tscn")
const DailySpeedle = preload("res://Scenes/DailySpeedle.tscn")
const InfiniteSpeedle = preload("res://Scenes/InfiniteSpeedle.tscn")
const HistoryScene = preload("res://Scenes/HistoryScene.tscn")
const Tutorial = preload("res://Scenes/Tutorial.tscn")
const Statistic = preload("res://Scenes/StatisticsView.tscn")

 
var guessed_words = {}
var all_words = []
var historic_data : Dictionary

func _ready():
	$Networking.hide()
	$OverlayCanvas.hide()
	load_from_disc("user://progress.save")
	generate_wordlist()
	Signals.connect("show_historic_data", self, "show_history")


func generate_wordlist(letters=5):
	# For future compatibility the length of the words can be set
	# Currently only 5 letter words are supported
	var file = File.new()
	var file2 = File.new()
	if letters == 5:
		file.open("res://Assets/Words/valid_guesses.csv", File.READ)
		file2.open("res://Assets/Words/fiveLetters_word_list_english.csv", File.READ)
	elif letters == 6:
		file.open("res://Assets/Words/sixLetters_word_list_english.csv", File.READ)
	while !file.eof_reached():
		var content = file.get_csv_line()
		all_words.append(content[0].to_lower())
	while !file2.eof_reached():
		var content = file2.get_csv_line()
		if not content[0].to_lower() in all_words:
			all_words.append(content[0].to_lower())
	file.close()


func start_game():
	close_all_panels()
	var word_field = WordsField.instance()
	self.add_child(word_field)


func start_daily_game():
	close_all_panels()
	var daily_speedle = DailySpeedle.instance()
	if daily_done():
		daily_speedle.done_already = true
	self.add_child(daily_speedle)


func start_infinite_game():
	close_all_panels()
	var infinite_speedle = InfiniteSpeedle.instance()
	self.add_child(infinite_speedle)
	if "Infinite" in guessed_words:
		var max_streak = guessed_words["Infinite"]["maxStreak"]
		infinite_speedle.max_streak = max_streak


func show_history(data):
	var gb = _get_game_board()
	if not gb == null:
		gb.queue_free()
	historic_data = data
	var historic_scene = HistoryScene.instance()
	self.add_child(historic_scene)


func daily_done():
	var td = OS.get_date(true)
	td = "%04d-%02d-%02d" % [td.year, td.month, td.day]
	if "Daily" in guessed_words:
		for game in guessed_words["Daily"]:
			if td in guessed_words["Daily"][game]:
				return true
	return false


remotesync func start_multiplayer_game():
	start_game()


func store_in_dict(result, word, guesses, game_type, today, time_left, streak=0, max_streak=0):
	var games_played : int
	if game_type in guessed_words:
		games_played = len(guessed_words[game_type])
	else:
		games_played = 0
		guessed_words[game_type] = {}
	if game_type == "Infinite":
		guessed_words[game_type]["maxStreak"] = max_streak
		guessed_words[game_type][games_played+1] = {"result": result,
										 "word": word,
										 "guesses": guesses,
										 today: result,
										 "time_left": time_left,
										 "streak": streak,
										 "hard": gamestate.hard_mode}
	else:
		guessed_words[game_type][games_played+1] = {"result": result,
										 "word": word,
										 "guesses": guesses,
										 today: result,
										 "time_left": time_left,
										 "hard": gamestate.hard_mode}
	save_on_disc(guessed_words)


func save_on_disc(item):
	var file = File.new()
	file.open("user://progress.save", File.WRITE)
	file.store_var(item)
	file.close()


func load_from_disc(what):
	var file = File.new()
	if file.file_exists(what):
		file.open(what, File.READ)
		guessed_words = file.get_var()
		file.close()
	else:
		pass


func close_all_panels():
	$MainMenu.hide()
	$Networking/WaitingPanel.hide()
	$Networking.hide()
	$Networking/ConnectionFailed.hide()
	$OverlayCanvas.hide()


func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ESCAPE:
			get_tree().quit()


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		_on_back_button()
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		_on_back_button()


func _on_back_button():
	var gb
	for child in self.get_children():
		if child is GameBoard:
			gb = child
	if self.has_node("Tutorial"):
		$Tutorial.remove()
	elif self.has_node("StatisticsView"):
		if gb:
			gb.back_to_menu()
		else:
			$StatisticsView.queue_free()
	elif gb:
		if gb.has_node("HistoryRect") or gb.get_node("GameEndWindow").is_visible_in_tree():
			gb.back_to_menu()
		else:
			gb.get_node("GameEndWindow").show()
	else:
		if get_node("Networking").is_visible_in_tree():
			get_node("Networking")._on_CancelServer_pressed()
		elif get_node("Credits").is_visible_in_tree():
			_on_CreditsCloseButton_pressed()
		else:
			get_tree().quit()


func _on_CreditsCloseButton_pressed():
	$Credits.hide()


func _on_TextureButton_pressed():
	_on_back_button()


func _on_HelpButton_pressed():
	close_open_widget()
	if not self.has_node("Tutorial"):
		var tutorial = Tutorial.instance()
		self.add_child(tutorial)


func _on_StatisticsButton_pressed():
	close_open_widget()
	if not self.has_node("StatisticsView"):
		var statistics = Statistic.instance()
		self.add_child(statistics)
		var gb = _get_game_board()
		if not gb == null:
			if not gb.get_node("TimerLabel/SoloGameTimer").is_stopped():
				statistics.get_node("WarningPanel").show()
			statistics.get_node("VBox/GameRunningLabel").show()
		statistics.add_statItems()


func close_open_widget():
	var possible_widgets = ["Tutorial", "StatisticsView"]
	for w in possible_widgets:
		if self.has_node(w):
			self.get_node(w).queue_free()


func _get_game_board():
	var gb
	for child in self.get_children():
		if child is GameBoard:
			gb = child
	return gb
