extends Button

var target_colour : Color

onready var tween = $ColourTween
signal button_pressed


func _ready():
	pass 


func _on_VKey_pressed():
	emit_signal("button_pressed", self.text)


func change_bg_colour(colour):
	tween.interpolate_property($bg_colour, "color", $bg_colour.color, colour, 1.0, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()


func get_bg_colour():
	return $bg_colour.color
