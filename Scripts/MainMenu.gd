extends Control


func _ready():
	pass


func _on_StartButton_pressed():
	get_node("..").start_game() 


func _on_MultiplayerButton_pressed():
	get_node("..").get_node("Networking").show()
	$".".hide()


func _on_DailyWordButton_pressed():
	get_node("..").start_daily_game()


func _on_CheckButton_toggled(button_pressed):
	if $CheckButton.pressed:
		gamestate.sound_on = true
	else:
		gamestate.sound_on = false


func _on_Button_pressed():
	get_parent().get_node("Credits").show()


func _on_InfiniteButton_pressed():
	get_parent().start_infinite_game()


func _on_HardModeButton_toggled(button_pressed):
	if $HardModeButton.pressed:
		gamestate.hard_mode = true
	else:
		gamestate.hard_mode = false
