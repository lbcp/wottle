extends Control

const Grey = preload("res://Scenes/Resources/GreyRect.tscn")
const Green = preload("res://Scenes/Resources/GreenRect.tscn")
const Yellow = preload("res://Scenes/Resources/YellowRect.tscn")

var data: Dictionary


func _ready():
	pass 


func create_view():
	var date
	for key in data.keys():
		if key != "result" and key != "word" and key != "guesses" and key != "time_left"\
											 					  and key != "hard":
			date = key
	$HBoxContainer/VBoxContainer/DateContainer/DateLabel.text = str(date)
	$HBoxContainer/VBoxContainer/ResultContainer/ResultLabel.text = data["result"]
	$HBoxContainer/VBoxContainer/TimeContainer/TimeLabel.text = str(data["time_left"])
	generate_patches(data["guesses"], data["word"])


func generate_patches(guesses, result):
	for guess in len(guesses):
		print(guesses[guess])
		var patch_pos = check_word(guesses[guess], result)
		for i in len(patch_pos):
			var patch
			if patch_pos[i] == "green":
				patch = Green.instance()
			elif patch_pos[i] == "grey":
				patch = Grey.instance()
			elif patch_pos[i] == "yellow":
				patch = Yellow.instance()
			$HBoxContainer/PatchBox.get_child(guess+1).add_child(patch)


func check_word(word: String, required_word: String):
	# This function was taken from the WordsField scene
	word = word.to_lower()
	required_word = required_word.to_lower()
	var word_dict = {}
	var letter_pos_col = {}

	for l in required_word:
		if l in word_dict:
			word_dict[l] += 1
		else:
			word_dict[l] = 1
	for i in range(5):
		if word[i] == required_word[i]:
			letter_pos_col[i] = "green"
			word_dict[word[i]] -= 1
		elif not word[i] in required_word:
			letter_pos_col[i] = "grey"
	# check for remaining yellow positions
	for i in range(5):
		if word[i] in required_word and not i in letter_pos_col:
			if word_dict[word[i]] > 0:
				letter_pos_col[i] = "yellow"
				word_dict[word[i]] -= 1
			else:
				letter_pos_col[i] = "grey"
	return letter_pos_col


func _on_GameStatButton_pressed():
	Signals.emit_signal("show_historic_data", data)
