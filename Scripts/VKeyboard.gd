extends Control

const Vkey = preload("res://Scenes/VKey.tscn")
var letters_list = ["qwertyuiop", "asdfghjkl", "zxcvbnm"]


func _ready():
	var del_button = Vkey.instance()
	del_button.text = "Del"
	del_button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
	del_button.connect("button_pressed", self.get_parent(), "_button_pressed")
	$Vbox/Letters3.add_child(del_button)
	
	for l in letters_list[0]:
		var button = Vkey.instance()
		button.text = l
		button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
		button.connect("button_pressed", self.get_parent(), "_button_pressed")
		$Vbox/Letters1.add_child(button)
		
	for l in letters_list[1]:
		var button = Vkey.instance()
		button.text = l
		button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
		button.connect("button_pressed", self.get_parent(), "_button_pressed")
		$Vbox/Letters2.add_child(button)
	for l in letters_list[2]:
		var button = Vkey.instance()
		button.text = l
		button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
		button.connect("button_pressed", self.get_parent(), "_button_pressed")
		$Vbox/Letters3.add_child(button)
	
	var ent_button = Vkey.instance()
	ent_button.text = "Enter"
	ent_button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
	ent_button.connect("button_pressed", self.get_parent(), "_button_pressed")
	$Vbox/Letters3.add_child(ent_button)

func disable_keys():
	for no in $Vbox.get_children():
		for bu in no.get_children():
			bu.disabled = true


func enable_keys():
	#Not used currently
	for no in $Vbox.get_children():
		for bu in no.get_children():
			bu.disabled = false


func reset_colours():
	for no in $Vbox.get_children():
		for bu in no.get_children():
			bu.get_child(0).color = Color(0.15, 0.17, 0.23, 1)
