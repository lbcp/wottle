extends Control

const GameStatItem = preload("res://Scenes/GameStatItem.tscn")

var guessed_words
var history

func _ready():
	$WarningPanel.hide()
	guessed_words = get_node("..").guessed_words["Daily"]
	history = crunching_numbers(guessed_words)
	$VBox/GeneralStatsContainer/GamesCount/GamesPlayed.text = str(history[0])
	$VBox/GeneralStatsContainer/WinsCount/Wins.text = history[1]
	$VBox/GeneralStatsContainer/LostCount/Lost.text = history[2]
	$VBox/GeneralStatsContainer/CurrentStreakCount/CurrentStreak.text = history[3]
	$VBox/GeneralStatsContainer/MaxStreakCount/MaxStreak.text = history[4]


func add_statItems():
	$ProgressBar.value = 0
	$ProgressBar.show()
	var progress_steps = 100 / history[0]
	var dict_keys = guessed_words.keys()
	dict_keys.invert()
	for item in dict_keys:
		var gamestatitem = GameStatItem.instance()
		gamestatitem.data = guessed_words[item]
		gamestatitem.create_view()
		$VBox/ScrollContainer/GamesHistoryContainer.add_child(gamestatitem)
		$ProgressBar.value += progress_steps
	$ProgressBar.hide()


func crunching_numbers(data) -> Array:
	var games_count = len(data)
	var wins = 0
	var losses = 0
	var current_streak = 0
	var max_streak = 0
	for i in data:
		if data[i]["result"] == "win":
			wins += 1
			current_streak += 1
			if max_streak <= current_streak:
				max_streak = current_streak
		else:
			losses += 1
			current_streak = 0
	return [games_count, str(wins), str(losses), str(current_streak), str(max_streak)]


func _on_Button_pressed():
	$WarningPanel.hide()
